const dotenv = require('dotenv')
dotenv.config()

module.exports = {
    HOST : process.env.PG_HOST,
    USERNAME : process.env.PG_USERNAME,
    PASSWORD : process.env.PG_PASSWORD,
    DATABASE_NAME : process.env.PG_DATABASE_NAME,
    PORT : process.env.PORT||5000,
    ADMIN_EMAIL : process.env.EMAIL,
    ADMIN_PASSWORD : process.env.PASSWORD,
    SECRET : process.env.SECRET_KEY
}