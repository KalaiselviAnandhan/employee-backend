const express = require("express")
const app = express()
const { PORT } = require("./config")
const table = require("./Connection/tableCreation")
const cors = require("cors")
const employee = require("./Routes/employee")
const admin = require("./Routes/admin")
const validation = require("./Routes/validation")

async function tablesCreation(){
    try{
        await table.adminTableCreation()
        await table.employeeTableCreation()
    }
    catch(error){
        console.error(error)
    }
}
tablesCreation()

app.use(cors())
app.use(express.json())

app.use('/employee',employee)
app.use('/admin',admin)
app.use('/validate',validation)

app.listen(PORT)