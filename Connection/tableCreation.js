const pool = require("./connection")
const config = require("../config")
const bcrypt = require('bcrypt')

const adminCreation = `CREATE TABLE IF NOT EXISTS empadmin(
    id INTEGER PRIMARY KEY NOT NULL, 
    email VARCHAR(200) NOT NULL,
    password VARCHAR(100) NOT NULL
);`

const adminValue = `INSERT INTO empadmin(id,email,password) VALUES(1,$1,$2);`

const employeeCreation = `
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE IF NOT EXISTS employee(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    empid VARCHAR(500) NOT NULL UNIQUE,
    name TEXT NOT NULL,
    age INTEGER NOT NULL,
    gender VARCHAR(100) NOT NULL,
    ph BIGINT NOT NULL UNIQUE,
    email VARCHAR(200) NOT NULL UNIQUE,
    address TEXT NOT NULL,
    city TEXT NOT NULL,
    qualification TEXT NOT NULL,
    designation TEXT NOT NULL,
    experience INTEGER NOT NULL,
    salary INTEGER NOT NULL,
    department TEXT NOT NULL,
    status BOOLEAN DEFAULT true NOT NULL);`

const employeeValues = `INSERT INTO employee(empid,name,age,gender,ph,email,address,city,qualification,designation,experience,salary,department) VALUES
    ('EMP20HR1','Anandhan',35,'male',1234567892,'anand@gmail.com','#7,vv.puram','Bangalore','MBA','Human Resource Officer',10,100000,'Human Resource'),
    ('EMP20HR2','Naga Anand',25,'male',1234567891,'naagaanand@gmail.com','#102,Basvanagudi','Bangalore','MBA','HR Assistant',3,70000,'Human Resource'),
    ('EMP20FI1','Subram',25,'male',1234567893,'subram@gmail.com','#112,Basvanagudi','Bangalore','MBA','Chief Financial Officer',18,150000,'Finance'),
    ('EMP20IT1','Manju',23,'male',1234567894,'anil@gmail.com','#182,JP Nagar','Bangalore','ME','Senior Developer',9,70000,'Information Technology');`

function adminTableCreation(){
    return new Promise((resolve, reject)=>{
        pool.query(adminCreation, function(error,result){
            if(error){
                reject(error)
            }
            pool.query(`SELECT COUNT(*) AS counter FROM empadmin`,(error,result)=>{
                if(error){
                    reject(error)
                }

                if(result.rows[0].counter === "0"){
                    bcrypt.hash(config.ADMIN_PASSWORD,10,function(error,hash){
                        if(error){
                            reject(error)
                        }
                        pool.query(adminValue, [config.ADMIN_EMAIL,hash], (error)=>{
                            if(error){
                                reject(error)
                            }
                            resolve()
                        })
                    })
                }
            })
        })
    })
}


function employeeTableCreation(){
    return new Promise((resolve, reject)=>{
        pool.query(employeeCreation, function(error,result){
            if(error){
                reject(error)
            }

            pool.query(`SELECT COUNT(*) AS counter FROM employee`,(error,result)=>{
                if(error){ 
                    reject(error)
                }

                if(result.rows[0].counter === "0"){
                    pool.query(employeeValues, (error)=>{
                        if(error){
                            reject(error)
                        }
                        resolve()
                    })
                }
            })
        })
    })
}

module.exports ={
    adminTableCreation,
    employeeTableCreation
} 
