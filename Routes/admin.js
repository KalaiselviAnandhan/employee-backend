const router = require("express").Router()
const pool = require('../Connection/connection')
const {check, body, validationResult} = require('express-validator')
const checkEmpty = (value) => check(value).isLength({min:1}).withMessage(`${value} Required`)
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const config = require('../config')

router.post('/',[
    checkEmpty('email'),
    body('email').isEmail(),
    checkEmpty('password'),
    body('password').isLength({min:8})
],(req,res)=>{
    const errors = validationResult(req)
    if (!errors.isEmpty()) { 
        return res.status(422).json({ errors: errors.array() })   
    }
    else{
        const login = `SELECT * FROM empadmin;` 
        pool.query(login,(error,result) => {
            if(error){
                res.status(500).send("Internal Server Error")
            }
            const loginData = result.rows
            if(loginData.length > 0 && loginData[0].email === req.body.email){
                bcrypt.compare(req.body.password, loginData[0].password, function(err, result){
                    if(err){
                        res.status(401).send("Unauthorized User Credentials Password doesn't match")
                    }
                    else if(result === true){
                        const tokenClaims = {
                            id: loginData[0].id,
                            isAdmin: loginData[0].email,
                        }
                        jwt.sign(tokenClaims, config.SECRET, (err, token) => {
                            if(err){
                                res.status(500).send("Internal Server Error")
                            }
                            else{
                                res.send({token})
                            }
                        })   
                    }
                    else{
                        res.status(401).send("Unauthorized User Credentials Password doesn't match")
                    }
                });
            }
            else{
                res.status(401).send("Unauthorized User Credentials")
            }
        })
    }
})
module.exports = router