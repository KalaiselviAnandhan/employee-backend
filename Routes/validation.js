const express = require("express")
const router = express.Router()
const pool = require('../Connection/connection')
const jwt = require('jsonwebtoken')
const config = require('../config')

const adminValidation = (req,res,next) => {
    const token = req.header('auth')
    req.token = token
    next()
}

router.get("/empid/:empid",adminValidation,(req,res) => {
    jwt.verify(req.token, config.SECRET, (err,data) => {
        if(err){
            res.status(403).send("Forbidden")
        }
        const query = `SELECT * FROM employee WHERE empid = $1`
        pool.query(query, [req.params.empid], (error,result) => {
            if(error){
                res.status(500).send("Internal Server Error")
            }
            (!result.rows.length) ? res.send("not exist") : res.status(422).send('exist')
        })
    })
})

router.get("/email/:mailId",adminValidation,(req,res) => {
    jwt.verify(req.token, config.SECRET, (err,data) => {
        if(err){
            res.status(403).send("Forbidden")
        }
        const query = `SELECT * FROM employee WHERE email = $1`
        pool.query(query, [req.params.mailId], (error,result) => {
            if(error){
                res.status(500).send("Internal Server Error")
            }
            (!result.rows.length) ? res.send("not exist") : res.status(422).send('exist')
        })
    })
})

router.get("/ph/:num",adminValidation,(req,res) => {
    jwt.verify(req.token, config.SECRET, (err,data) => {
        if(err){
            res.status(403).send("Forbidden")
        }
        const query = `SELECT * FROM employee WHERE ph = $1`
        pool.query(query, [req.params.num], (error,result) => {
            if(error){
                res.status(500).send("Internal Server Error")
            }
            (!result.rows.length) ? res.send("not exist") : res.status(422).send('exist')
        })
    })
})

module.exports = router