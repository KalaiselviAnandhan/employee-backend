const router = require("express").Router()
const pool = require('../Connection/connection')
const {check, body, validationResult} = require('express-validator')
const jwt = require('jsonwebtoken')
const config = require('../config')

const checkEmpty = (value) => check(value).isLength({min:1}).withMessage(`${value} Required`)

const adminValidation = (req,res,next) => {
    const token = req.header('auth')
    req.token = token
    next()
}

router.get("/",adminValidation,(req,res) => {
    jwt.verify(req.token, config.SECRET, (err,data) => {
        if(err){
            res.status(403).send("Forbidden")
        }
        const query = `SELECT * FROM employee`
        pool.query(query, (error,result) => {
            if(error){
                res.status(500).send("Internal Server Error")
            }
            res.send(result.rows)
        })
    })
})

router.get("/:id",adminValidation,(req,res) => {
    jwt.verify(req.token, config.SECRET, (err,data) => {
        if(err){
            res.status(403).send("Forbidden")
        }
        const query = `SELECT * FROM employee WHERE id = $1`
        pool.query(query, [req.params.id],(error, result) => {
            if(error){
                res.status(500).send("Internal Server Error")
            }
            res.send(result.rows)
        })
    })
})

router.get("/select/:dept",adminValidation,(req,res) => {
    jwt.verify(req.token, config.SECRET, (err,data) => {
        if(err){
            res.status(403).send("Forbidden")
        }
        const query = `SELECT * FROM employee WHERE department = $1`
        pool.query(query, [req.params.dept],(error, result) => {
            if(error){
                res.status(500).send("Internal Server Error")
            }
            res.send(result.rows)
        })
    })
})

router.get('/search/select',adminValidation,(req,res) => {
    jwt.verify(req.token, config.SECRET, (err,data) => {
        if(err){
            res.status(403).send("Forbidden")
        }
        const query = req.query.department === 'All' ? `SELECT * FROM employee WHERE name || gender || empid || department || designation ILIKE $1` : `SELECT * FROM employee WHERE name || gender || empid || department || designation ILIKE $1 AND department = $2`
        const value = req.query.department === 'All' ? [`%${req.query.key}%`] : [`%${req.query.key}%`,req.query.department]
        pool.query(query, value,(error, result) => {
            if(error){
                console.log('error',error)
                return res.status(500).send("Internal Server Error")
            }
            res.send(result.rows)
        })
    })
})


router.post("/",[
    checkEmpty('empid'),
    body('empid').isAlphanumeric(),
    checkEmpty('name'),
    checkEmpty('age'),
    checkEmpty('gender'),
    checkEmpty('ph'),
    body('ph').isLength({min:10}),
    body('email').isEmail(),
    checkEmpty('address'),
    checkEmpty('city'),
    checkEmpty('qualification'),
    checkEmpty('designation'),
    checkEmpty('experience'),
    checkEmpty('salary'),
    checkEmpty('department'),
    checkEmpty('status')
],adminValidation,(req,res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) { 
        return res.status(422).json({ errors: errors.array() })   
    }
    else{
        jwt.verify(req.token, config.SECRET, (err,data) => {
            if(err){
                res.status(403).send("Forbidden")
            }
            const query = `INSERT INTO employee(empid,name,age,gender,ph,email,address,city,qualification,designation,experience,salary,department,status) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)`
            pool.query(query, [req.body.empid, req.body.name, req.body.age, req.body.gender, req.body.ph, req.body.email, req.body.address, req.body.city, req.body.qualification, req.body.designation,req.body.experience,req.body.salary,req.body.department,req.body.status], 
            (error, result)=>{
                if(error){
                    res.status(500).send("Internal Server Error")
                }
                res.send(result)
            })
        })
    }
})

router.put('/:id',[
    checkEmpty('empid'),
    body('empid').isAlphanumeric(),
    checkEmpty('name'),
    checkEmpty('age'),
    checkEmpty('gender'),
    checkEmpty('ph'),
    body('ph').isLength({min:10}),
    body('email').isEmail(),
    checkEmpty('address'),
    checkEmpty('city'),
    checkEmpty('qualification'),
    checkEmpty('designation'),
    checkEmpty('experience'),
    checkEmpty('salary'),
    checkEmpty('department'),
    checkEmpty('status')
],adminValidation,(req,res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) { 
        return res.status(422).json({ errors: errors.array() })   
    }
    else{
        jwt.verify(req.token, config.SECRET, (err,data) => {
            if(err){
                res.status(403).send("Forbidden")
            }
            const query = `UPDATE employee SET empid = $1, name = $2, age = $3, gender = $4, ph = $5, email = $6, address = $7, city = $8, qualification = $9, designation = $10, experience = $11, salary = $12, department = $13, status = $14  WHERE id = $15`
            pool.query(query, [req.body.empid, req.body.name, req.body.age, req.body.gender, req.body.ph, req.body.email, req.body.address, req.body.city, req.body.qualification, req.body.designation,req.body.experience,req.body.salary,req.body.department,req.body.status, req.params.id], 
            (error, result) => {
                if(error){
                    res.status(500).send("Internal Server Error")
                }
                res.send(result)
            })
        })
    }
})

router.delete('/:id',adminValidation,(req,res) => {
    jwt.verify(req.token, config.SECRET, (err,data) => {
        if(err){
            res.status(403).send("Forbidden")
        }
        const query = `DELETE FROM employee WHERE id = $1`
        pool.query(query, [req.params.id], (error,result) => {
            if(error){
                res.status(500).send("Internal Server Error")
            }
            res.send(result)
        })
    })
})

module.exports = router